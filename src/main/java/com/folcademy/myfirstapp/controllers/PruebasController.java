package com.folcademy.myfirstapp.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class PruebasController {

    @PostMapping("/prueba")
    public String pruebaPost(){
        return "Prueba POST";
    }

    @GetMapping("/prueba")
    public String pruebaGet(){
        return "Prueba GET";
    }

    @DeleteMapping("prueba")
    public String pruebaDelete(){
        return "Prueba DELETE";
    }

    @PutMapping("prueba")
    public String pruebaPut(){
        return "Prueba PUT";
    }
}

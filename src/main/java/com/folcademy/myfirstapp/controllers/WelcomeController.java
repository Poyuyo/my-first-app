package com.folcademy.myfirstapp.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {

    @RequestMapping("/")
    public String saludar( @RequestParam(value="name", required=false, defaultValue="usuario") String nombre){
        //Probando con tags html
        return "<h1>Hola <i>"+nombre+"</i>, envia tu nombre en la url (?name=nombre)</h1>";
    }

}
